# Security Policy

## Supported Versions

We release patches for security vulnerabilities in the following versions:

| Version | Supported          |
| ------- | ------------------ |
| 1.0.x   | :white_check_mark: |
| 0.9.x   | :white_check_mark: |
| < 0.9   | :x:                |

## Reporting a Vulnerability

If you find a vulnerability in this project, please let us know immediately. We appreciate your efforts to responsibly disclose your findings and will make every effort to acknowledge your contributions.

To report a vulnerability, please follow these steps:

1. **Do not open an issue directly**. Instead, email us directly at [security@example.com](mailto:security@example.com) with the following details:
    - Description of the vulnerability and its potential impact.
    - Detailed steps to reproduce the vulnerability.
    - Any potential mitigation or patch suggestions, if available.

2. **Expect an initial response within 24 hours**. We will confirm the receipt of your email and provide an initial assessment.

3. **Work with us to resolve the issue**. We may request additional information or assistance to reproduce and fix the vulnerability. Your cooperation is essential for a timely resolution.

4. **Coordinate public disclosure**. Once the vulnerability is resolved, we will coordinate with you on public disclosure. We aim to release security patches and advisories as soon as possible while ensuring the fix is robust.

## Security Best Practices

To help us maintain a secure codebase, please adhere to the following best practices:

- **Stay Updated**: Keep your local repository up-to-date with the latest changes from the upstream repository.
- **Code Reviews**: Submit your changes via pull requests and request reviews from other team members.
- **Static Analysis**: Run static analysis tools to catch potential vulnerabilities early in the development process.
- **Dependency Management**: Regularly update your dependencies to include the latest security patches.

Thank you for helping us keep our project secure.
